#!/usr/bin/env python
"""
# Copyright 2012 Jeff Hsu.  All Rights Reserved

Load the gwas catalog into the django database
"""

__author__ = 'jeffhsu3@gmail.com'

import urllib
import pandas as pd
#from django.core.management import setup_environ
#from labme import settings

#setup_environ(settings)

GWAS_CATALOG_URL = 'http://www.genome.gov/admin/gwascatalog.txt'

from labme.saresity.models import Phenotype, Variant, Publication, Association

def load_gwas(filename):
    ''' Loads GWAS catalogue from NGHRI gwas catalogue.
    '''
    k = pd.read_csv(filename, sep="\t", skip_footer=1)
    for t in range(k.shape[0]):
        go = True
        try:
            # Best to put this validation in a form
            snp = k.ix[t, 'SNPs']
            snp = int(snp[2:])
            chrom = int(k.ix[t, 'Chr_id'])
            trait = k.ix[t, 'Disease/Trait']
        except ValueError:
            print('Not formatted well')
            go = False
        if trait == '':
            go = False
        if go:
            pp = Phenotype.objects.filter(name = trait)
            if pp:
                pp = pp[0]
            else:
                pp = Phenotype(name = trait, desc ='')
                pp.save()
            vp = Variant.objects.filter(rsID = snp)
            if vp and len(vp) == 1:
                vp = vp[0]
            else:
                vp = Variant(rsID = snp,
                                chrom = chrom,
                                position = int(k.ix[t, 'Chr_pos']))
                vp.save()
            ap = Association.objects.create(phenotype = pp,
                                            variant = vp,
                                            direction = True,
                                            beta = 0,
                                            mechanism = 1,
                                            evidence_level = 1
                                            )
        else: pass
'''

def main():
    import optparse
    p = optparse.OptionParser(__doc__)
    p.add_option("-D", "--debug", action='store_true', dest="D", help="debug")
    options, args = p.parse_args()

    k = pd.read_csv(args[0], sep="\t")
    trait = set(k['Disease/Trait'])
    # :TODO the gwas database catalogue is kinda stupid.  Phenotypes need to
    # be better described
    for i in trait:
        print(i)
        d = Phenotype(name = i, desc = '')
        d.save()

'''

if __name__ == '__main__':
    pass
    #main()
