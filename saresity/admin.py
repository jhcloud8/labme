from saresity.models import (Association, Publication, 
        Variant, Phenotype)
from django.contrib import admin

admin.site.register(Association)
admin.site.register(Publication)
admin.site.register(Variant)
admin.site.register(Phenotype)
