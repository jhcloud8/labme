""" App that stores association data, (more than just GWAS) 
"""

from django.db import models
from django.contrib.auth.models import User


class Variant(models.Model):
    """ Only the relevent portions of dbSNP are kept.  
    """
    rsID = models.IntegerField(primary_key=True)
    kentbin = models.IntegerField()
    chrom = models.IntegerField()
    position = models.IntegerField()
    variant = models.CharField(max_length=250)
    def __unicode__(self):
        return str(self.rsID)


class Phenotype(models.Model):
    """ Phenotype is a heritable trait with some underlying genetic basis.
    Phenotypes are machine learned from PubMed
    """
    name = models.CharField(max_length=250)
    desc = models.TextField(max_length = 20000)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)
    assoc_vars = models.ManyToManyField(Variant, through='Association') 

    def __unicode__(self):
        return self.name


class Publication(models.Model):
    """ Basic publication model
    """
    pub_file = models.FileField(upload_to='saresity/pubs/')
    title = models.CharField(max_length = 300)
    created = models.DateTimeField(auto_now_add=True)
    url = models.URLField()

    def __unicode__(self):
        return self.title


class Association(models.Model):
    """ The choices for mechanism are machine learned
    Evidence level for that the variant is indeed causal
    """
    phenotype = models.ForeignKey(Phenotype)
    variant = models.ForeignKey(Variant)
    beta = models.IntegerField(blank = True)
    publication = models.ManyToManyField(Publication)
    # Positive Association = True 
    direction = models.BooleanField() 
    mechanism = models.IntegerField(blank = True)
    evidence_level = models.IntegerField(blank = True)
    created = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        out_string = str(self.variant.rsID) + ' with ' + self.phenotype.name
        return(out_string)


class TALEN(models.Model):
    pass


class CRISPR_homolog(models.Model):
    id = models.IntegerField(primary_key=True)
    kent_bin = models.IntegerField()
    sequence = models.IntegerField(max_length=250)
    start = models.IntegerField()
    chrom = models.IntegerField()


class CRISPR_target(models.Model):
    id = models.IntegerField(primary_key=True)
    kent_bin = models.IntegerField()
    sequence = models.IntegerField(max_length=250)
    start = models.IntegerField()
    chrom = models.IntegerField()
    verified = models.BooleanField(default=False)
