# Create your views here.

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse

from saresity.models import Phenotype, Association, Variant

@login_required(login_url='/weblab/login/')
def index(request):
    return render_to_response('hNP.html', 
            context_instance=RequestContext(request))
    
