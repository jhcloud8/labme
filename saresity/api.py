from django.contrib.auth.models import User
from django.conf.urls.defaults import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie import fields
from tastypie.validation import CleanedDataFormValidation
from tastypie.utils import trailing_slash
from haystack.query import SearchQuerySet

from saresity.models import (Publication, Association, Variant,
        Phenotype, CRISPR_homolog, CRISPR_target)

class SessionAuthentication(BasicAuthentication):
    def __init__(self, *args, **kwargs):
        super(SessionAuthentication, self).__init__(*args, **kwargs)

    def is_authenticated(self, request, **kwargs):
        from django.contrib.sessions.models import Session
        if 'sessionid' in request.COOKIES:
            s = Session.objects.get(pk=request.COOKIES['sessionid'])
            if '_auth_user_id' in s.get_decoded():
                u = User.objects.get(id=s.get_decoded()['_auth_user_id'])
                request.user = u
                #print('_auth_user_id found: %s' % u)
                return True
        return super(SessionAuthentication, self).is_authenticated(request, **kwargs)

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_active', 'is_staff',
        'is_superuser']
        #User Identification
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()
