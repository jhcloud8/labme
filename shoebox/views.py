from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect
from shoebox.models import Post

def index(request):
    posts = Post.objects.order_by("-created")
    if len(posts)>=10: posts = posts[0:10]
    return(render_to_response('shoebox.html', {'posts':posts,}, 
        context_instance=RequestContext(request)))
