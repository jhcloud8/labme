from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^weblab/', include('weblab.urls')),
    (r'^hDP/', include('hDP.urls')),
    (r'^genome/', include('genome.urls')),
    (r'^accounts/', include('registration.backends.default.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'^login/', include(admin.site.urls)),
    (r'^shoebox/', include('shoebox.urls')),
    (r'^eQTL/', include('eQTL.urls')),
    (r'^hNP/', include('saresity.urls')),
    (r'^search/', include('haystack.urls')),
    (r'^$', include('shoebox.urls')),
)
