from django.conf.urls.defaults import *
from hDP import views

urlpatterns = patterns('hDP.views',
        (r'^test/', "test"),
        (r'^test2/', "test2"),
        (r'^$', "index"),
)
