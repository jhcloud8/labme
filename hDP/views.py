"""
Views for the webGl globe and the Human Geneome Diversity Panel
"""
import json, random
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from hDP.models import Population, Individual
from forms import SNPForm

def index(request):
    populations = Population.objects.all()
    if request.method == 'POST':
        rs_query = request.POST['rsID']
        response = [[random.random(), pop.latitude, 
            pop.longitude]for pop in populations]
        print(len(response))
        response = json.dumps(response)
        return(HttpResponse(response, mimetype="application/json"))
    else:
        response = [[random.random(), pop.latitude, pop.longitude]for pop in populations]
        response = json.dumps(response)
        form = SNPForm()
        return render_to_response('webgl.html',{'data':response, 'form':form,},
                context_instance=RequestContext(request))


def test(request):
    return(render_to_response('test.html',
        context_instance=RequestContext(request)))

def test2(request):
    return(render_to_response('test2.html',
        context_instance=RequestContext(request)))
