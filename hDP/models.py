from django.db import models

# Create your models here

class Population(models.Model):
    """
    Table containing information about the various HGDP populations
    """
    population_name = models.CharField(max_length=250)
    nickname = models.CharField(max_length=3)
    latitude = models.SmallIntegerField()
    longitude = models.SmallIntegerField()
    comment = models.CharField(max_length=250)

    def __unicode__(self):
        return(self.population_name)

class Individual(models.Model):
    """
    Table maping HGDP IDS to population as well as gender
    Ignore origin because except for a few cases all come from origin
    """
    GENDER = (
            (0, 'Male'),
            (1, 'Female')
            )
    hgdp_id = models.CharField(max_length=10, primary_key = True)
    gender = models.SmallIntegerField(choices=GENDER)
    population_name = models.ForeignKey(Population)

    def __unicode__(self):
        return(self.hgdp_id)

class Genotypes(models.Model):
    """
    Oh god so much data.  See if you can use pyTables for this
    """
    pass

class Phenotypes(models.Model):
    pass


