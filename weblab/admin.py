from weblab.models import (Location, Sample, Animal, Storage,
                           Experiment, Lab, Sequence, SequenceFolder,
                           SeqFeature, FeatureLocation)
from django.contrib import admin

admin.site.register(Storage)
admin.site.register(Location)
admin.site.register(Sample)
admin.site.register(Animal)
admin.site.register(Experiment)
admin.site.register(Lab)
admin.site.register(Sequence)
admin.site.register(SequenceFolder)
admin.site.register(FeatureLocation)
admin.site.register(SeqFeature)
