class SQ.Models.SeqFolder extends Backbone.RelationalModel
    url: () ->
        return this.get('resource_uri') || this.collection.url

    relations: [
        {
            type: 'HasMany',
            key: 'sequences',
            relatedModel: 'seqRel',
            reverseRelation: {
                key: 'folderSeq'
            }
        }
    ],

    initialize: () ->


class SQ.Models.SeqRel extends Backbone.RelationalModel
    defaults: {
    }


class SQ.Collections.SeqFolderCollection extends Backbone.Collection
    
    model: SeqFolder,
    
    url: '/weblab/api/v1/sequence_folder/',

    parse: (data) -> {
        return data.objects
    },

