class SQ.Models.Seq extends Backbone.Model
    url: () ->
        return this.get('resource_uri') || this.collection.url
    
    initialize: () ->
    """ 
    relations: [{
        type: Backbone.HasMany,
        key: 'featurelocation',
        relatedModel: 'FeatureLocation',
        collectionType: 'FeatureLocationCollection',
        reverseRelation: {
            key: 'sequence'
            },
    }]
    """


"""
window.inFolder = Backbone.RelationalModel.extend({
    defaults: {
    }
})
"""


class SQ.Collections.SeqCollection extends Backbone.Collection
    model: SQ.Models.Seq,
    url: '/weblab/api/v1/sequence/',
    parse: (data) ->
        return data.objects


class SQ.Models.SeqFolder extends Backbone.Model
    # Was Backbone-Relational.  Need to make fixes
    url: () ->
        return this.get('resource_uri') || this.collection.url
    initialize: () ->
        #pass
    relations: [
            type: Backbone.HasMany,
            key: 'sequences',
            relatedModel: 'inFolder',
            reverseRelation:
                key: 'folder'
    ]


class SQ.Collections.SeqFolderCollection extends Backbone.Collection
    model: SQ.Models.SeqFolder,
    url: '/weblab/api/v1/sequence_folder/',
    parse: (data) ->
        return data.objects


class SQ.Models.SeqFeature extends Backbone.Model
    url: () ->
        return this.get('resource_uri') || this.collection.url
    initialize: () ->
        _.bindAll(this)


class SQ.Collections.SeqFeatureCollection extends Backbone.Collection
    model: SQ.Models.SeqFeature,
    url: '/weblab/api/v1/seq_feature/',
    parse: (data) ->
        return data.objects
    

class SQ.Models.FeatureLocation extends Backbone.Model
    url: () ->
        return this.get('resource_uri') || this.collection.url

    validate: (attrs) ->
        if attrs.end < attrs.start
            return "end must come after start"

    initialize: () ->
        _.bindAll(this)
    

class SQ.Collections.FeatureLocationCollection extends Backbone.Collection
    model: SQ.Models.FeatureLocation,
    url: '/weblab/api/v1/featurelocation/',
    parse: (data) ->
        return data.objects

