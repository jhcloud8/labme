var submenu = 0;
var timeout    = 200;
var closetimer = 0;

function nav_open()
{  nav_canceltimer();
   nav_close();
   submenu = $(this).find('ul').css('visibility', 'visible');}

function nav_close()
{  if(submenu) submenu.css('visibility', 'hidden');}

function nav_timer()
{  closetimer = window.setTimeout(nav_close, timeout);}

function nav_canceltimer()
{  if(closetimer)
   {  window.clearTimeout(closetimer);
      closetimer = null;}}

$(document).ready(function()
{  $('#nav > li').bind('mouseover', nav_open)
   $('#nav > li').bind('mouseout',  nav_timer)});

document.onclick = nav_close;
