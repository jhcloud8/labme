// 
// Copyright Jeffrey Hsu 2012

// Seq Collection View
// Colleciton of folders under a sequence
window.SequencesView = Backbone.View.extend({

    events: {
        'click .delete_sequence': 'deleteOne'
    },

    initialize: function () {
        _.bindAll(this, 'addOne', 'addAll', 'deleteOne', 'render', 'unrender');
        this.collection.bind('add', this.addOne);
        this.collection.bind('reset', this.addAll);
        this.collection.bind('all', this.render);
    },

    addAll: function () {
        this.collection.each(this.addOne);
    },

    addOne: function (sequence) {
        var view = new sequenceView({model:sequence});
        this.$('#sequences').append(view.render().el);
    },

    render: function () {

    },

    unrender: function() {
        this.$('#sequences').empty();
    },

    deleteOne: function () {
        //this.sequences.remove(currentEditing.model);
        $("#confirmOverlay").dialog({
            title: 'Confirm Delete',
            buttons: {
                "Yes": function() {
                    $(currentEditing.el).remove();
                    //Removes model and sends to persistance layer
                    currentEditing.model.destroy();
                    editingView.unrender();
                    $(this).dialog("close");
                    $('#current_edit').empty();
                },
                "No": function() {
                    $(this).dialog("close");
                },
            }
        });
    },
});


window.sequenceView = Backbone.View.extend({
    tagName: 'li',
    className: 'sequence',
    events: {
        'click a.seqname': 'edit_seq',
    },

    initialize: function() {
        _.bindAll(this, 'render','unrender' ,'edit_seq');
        this.bind('remove', 'unrender');
    },
    
    unrender: function() {
        $(this.el).remove();
    },

    edit_seq: function() {
        // Need to store editingView in global space
        if (window.editingView){
            editingView.remove();
        }

        $('.selected').removeClass('selected')
        $(this.el).addClass('selected')
        
        $('#app').append('<div id="edit_seq" class="edit"></div>')
        window.editingView = new editView({ el: $("#edit_seq"), 
                                          model: this.model});
        // Clear canvas
        editingView.render();
        //$('#edit_seq').append(editingView.render().el);
        //$('#edit_seq').html(ich.editTemplate(this.model.toJSON()));
        delete currentEditing;
        window.currentEditing = this;
    },

    render: function (){
        $(this.el).html(ich.sequenceTemplate(this.model.toJSON()));
        return this;
    },


});

window.f_l_Views = Backbone.View.extend({
    events:{
    },

    initialize: function() {
        _.bindAll(this, 'addOne', 'addAll', 'render');
        this.feature_locations = new FeatureLocationCollection();
        this.feature_locations.bind('add', this.addOne);
        this.feature_locations.bind('refresh', this.addAll);
        this.feature_locations.bind('all', this.render)'
    },

    addOne: function() {
    }, 

    addAll: function() {
        this.samples.each(this.addOne);
    },

    render: function() {
    },

});

window.f_l_View = Backbone.Views.extend({
});

window.featurelocationView = Backbone.View.extend({
    render: function() {
    },
});

window.editView = Backbone.View.extend({
    
    events: {
        'click button.add_feat': 'add_feature_sequence',
        'click button.search_sequence': 'search',
        'click button.delete_sequence': 'deleteOne', 
        'click button.edit_sequence': 'editSeq',
        'click button.toggle_feature_menu' : 'toggle_features',
    },
    
    initialize: function() {
        _.bindAll(this, 'select_seq_2', 'render', 'unrender', 'deleteOne', 
                  'add_feature_sequence', 'search', 'editSeq',
                  'toggle_features');
    },
    
    render: function() {
        // Settings
        var svdiag, seq_parse, d_length = 380;
        // Templating
        $(this.el).html(ich.editAppTemplate());
        $('#current_edit').append(this.model.get('name'));
        $('#feature_menu').menu().hide();
       
        // Render the sequence
        seq_parse = this.model.get('seq');
        this.render_seq(seq_parse);
        
        // Backbone relationships should be used here but...   
        this.feature_location_coll = new FeatureLocationCollection();
        var featurelocations = this.model.get('featurelocations');
        this.feature_location_coll.add(_.map(featurelocations, function (fl) { 
            fl.feature = fl.feature.resource_uri;
            var feature_loc = new FeatureLocation(fl);
            return feature_loc
        }));
        var d_ratio = d_length/this.seq_length; 
        console.log(this.feature_location_coll);
        
        // SVG implementation
        var d_y = 200;
        svgdiag = document.getElementById('svgdiag');
        while (svgdiag.lastChild){
        !
        var DNA_backbone = makeSVG('rect', {x: 5, y: d_y+4.0, width: '95%', height: '3px', fill: "#FCF7BD",
                                   stroke: 'black'});
        svgdiag.appendChild(DNA_backbone);

        // Need to add an overlap handler
        // :TODO make this its own view
        // Feature Renderer
        for (var k = 0; k < featurelocations.length; k++){
            var feature = featurelocations[k];
            var fstart = feature.start*d_ratio;
            var flength = (feature.end-feature.start)*d_ratio;
            // Generate path
            newpath = document.createElementNS("http://www.w3.org/2000/svg", "path");
            newpath.setAttribute('id', "DNA_feature" + feature.feature.name);
            newpath.setAttribute('fill', '#66FF99');
            newpath.setAttribute('stroke', 'grey');
            var fstart_m = fstart + 5;
            // Path generation
            var d_path = "M" + fstart_m + " " + d_y + " h " + flength + " v " + 10 + " h  -" + flength + "Z";
            newpath.setAttribute('d',  d_path);
            //var DNA_path_test = "<path d='M" + 5 + fstart " " + d_y + "H " + f_length + "V " + 10 "Z' fill='cyan' stroke='black'";
            //svgdiag.appendChild(DNA_feature);
            svgdiag.appendChild(newpath);
            newpath.onmousedown = function() {
                console.log('Feature Clicked:' + fstart);
            }

        }
        // Binding selection
        //$("#highlight").bind({mouseup: this.select_seq_2});
        $("#highlight").bind({mousemove: this.select_seq_2});
        return this;
    },

    toggle_features: function() {
        /* Toggles feature edit options
         * :TODO move this to bootstrap
         */
        $('#feature_menu').show().position({
            my: "left top",
            at: "left bottom",
            of: $('.toggle_feature_menu'),
        });
        $( window ).one( "click", function() {
            $('#feature_menu').hide();
        });  
        $('#feature_add').bind('click', editingView.add_feature_sequence);
        return false;

    },
    select_seq_2: function(ev) {
        $('#s_info').empty();
        var sel_obj = window.getSelection();
        var sel_seq = sel_obj.toString().replace(/(\r\n|\n|\r)/gm,"");
        var interval = sel_seq.length;
        var query_match = this.model.get('seq').search(sel_seq);
        var end = query_match + interval;

        $('#s_info').append(query_match + ".." + end + "(" + interval + ")");

    },
    
    render_seq: function(seq){
        this.seq_length = seq.length;
        var num_lines = Math.ceil(seq.length/50);
        this.num_lines = num_lines;
        for(var i=0; i < num_lines; i++){
            // :TODO add spaces to numbering, ie a space at every 10
            // Should be done with a span or something that doesn't insert text
            // That way the user can easily copy the text
            t_seq = this.model.get('seq').slice(i*50, (i+1)*50);
            var row = ich.editLines({"seq":t_seq, "row":i});
            var nums = ich.editNums({"nums":i*50+1});
            $("#line_numbers").append(nums);
            $("#highlight").append(row);
        }
    },

    render_diagram: function(features){
        // :TODO move rendering diagram here
    },

    unrender: function (){
        $('#line_numbers').empty();
        $('#highlight').empty();
    }, 

    deleteOne: function () {
        $("#confirmOverlay").dialog({
            title: 'Confirm Delete',
            buttons: {
                "Yes": function() {
                    $(currentEditing.el).remove();
                    currentEditing.model.destroy();
                    editingView.unrender();
                    $(this).dialog("close");
                    $('#current_edit').empty();
                    // :FIX Only updates one seq_collection, why?
                },
                "No": function() {
                    $(this).dialog("close");
                },
            },
        });
    },

    search: function () {
        /* Searches the string and highlights the position.
         *
         * Currently searches the sequence string stored in the model
         * and calculates the position within the text.
         */
        /*
        if (reverse_complement){
            //:TODO Implement reverse complement search
        }
        */
        
        // :TODO smart protein -> DNA -> protein searching
        // :TODO replace spaces
        // :TODO IAUPUC searching
        var q_search = $('#seqSearch').val().replace(/(\r\n|\n|\r)/gm,"");

        this.unrender();
        this.render_seq(this.model.get('seq'));
        var l_q = q_search.length;
        var qmatch = this.model.get("seq").search(q_search);
        var search_line = this.model.get("seq").length;
        
        var line_num = Math.ceil(qmatch/50)-1; 
        var remainder_of_line = qmatch%50;
        var num_lines = Math.ceil((qmatch + l_q)/50) - line_num;

        /*
        console.log('New_search'); 
        console.log(q_search.length);
        console.log('Number of lines:');
        console.log(num_lines);
        */
        // console.log(this.model.get("seq"));
        
        var list_of_lines = new Array();
        var remaining = l_q;
        for (var i=0; i< num_lines; i ++){
            current_line = line_num + i;
            if (i == 0){
                position = remainder_of_line;
                if (l_q > (50-remainder_of_line)) {
                    bp_span = 50 - position;
                } else {
                    bp_span = l_q;
                }
            } else if (i == num_lines - 1){
                position = 0;
                bp_span = remaining;
            } else {
                position = 0;
                bp_span = 50;
            }
            remaining = remaining - bp_span;
            list_of_lines[i] = ['#'+current_line+'.edit_row', position, bp_span];
        };
        //console.log(list_of_lines);
        
        // Generate new lines
        _.map(list_of_lines, function(lmi) { 
            // Line match info (lmi)
            var current_line = $(lmi[0]).text();
            var out_line = current_line.slice(0, lmi[1]) + '<span class="highlight">' +
                current_line.slice(lmi[1], lmi[1]+lmi[2]) + '</span>' + 
                current_line.slice(lmi[1] + lmi[2], 50);
            $(lmi[0]).html(out_line);
            //console.log(current_line.slice(lmi[1], lmi[1]+lmi[2]));
        });
    },

    add_feature_sequence: function() {
        var new_feat_bool = new Boolean();
        self = this;
        new_feat_bool = 0;
        // :TODO Reduce server-side calls here
        var seqfeats = new SeqFeatureCollection();
        seqfeats.fetch({
            success: function(collection, response){
                var features = $("select#addFeatureType");
                $.each(collection.models, function(index, feature){
                    features.append($("<option />").val(feature.cid).text(this.get('name')));
                });
            },
            error: function(collection, reponse){
            },
        });
        self.seqfeats = seqfeats;

        $('#newFeature').click(function () {
            // Template 
            // Simplfy this
            new_feat_bool = 1;
            $('#newFeature').after('<label>Name<textarea id="newFeatureName"></textarea></label>');
            $('#newFeature').after('<button id="availfeature">Features</button>');
            $('#newFeatureName').parent().after('<label>Description<textarea id="newFeatureDesc"></textarea></label>')
            $('#addFeatureType').remove();
            $('#newFeature').remove();
            $('#availfeature').click(function (){ 
                $('#addfeatureOverlay')
                .prepend('<label> Feature <select id = "addFeatureType"></select></label>');
                $('#addFeatureType').parent().after('<button id="newFeature">New Feature</button>');
                $('#newFeatureDesc').parent().remove();
                $('#newFeatureName').parent().remove();
                $('#availfeature').remove();
                self.add_feature_sequence();
            });

        });

        $('#addfeatureOverlay').dialog({
            width: 450,
            title: 'Add feature',
            buttons: {
                "Add": function() {
                    var temp = $(this);
                    // If new feature is set create a new feature first and get its resource_uri
                    var f_locs = new FeatureLocationCollection();
                    // To add the through model
                    if (new_feat_bool) {
                        f_locs.create({
                            name: $('#feature_name').val(),
                            start: $('#feature_start').val(), 
                            end: $('#feature_end').val(), 
                            orientation: $('#f_orientation').val(),
                            feature: {
                                name: $('#newFeatureName').val(),
                                description: $('#newFeatureDesc').val(),
                            },
                            sequence:self.model.get('resource_uri'),
                        },
                        {
                            success: function(model, error, options){
                                $('#feature_name').val(''),
                                $('#feature_start').val(''), 
                                $('#feature_end').val(''), 
                                $('#f_orientation').val(''),
                                temp.dialog("close");
                            },
                            error: function(model, error, options){
                            }
                        });
                    }else{
                        var feat_conv = seqfeats.getByCid($("select#addFeatureType").val());
                        f_locs.create({
                            name: $('#feature_name').val(),
                            start: $('#feature_start').val(), 
                            end: $('#feature_end').val(), 
                            orientation: $('#f_orientation').val(),
                            feature: feat_conv.get('resource_uri'),
                            sequence:self.model.get('resource_uri'),
                        },
                        {
                            success: function() {
                                $('#feature_name').val(''),
                                $('#feature_start').val(''), 
                                $('#feature_end').val(''), 
                                $('#f_orientation').val(''),
                                temp.dialog("close");
                            }

                        });
                    }
                    
                },
                "Cancel": function() {
                    if (new_feat_bool == 1) {
                        $('#addfeatureOverlay')
                        .prepend('<label> Feature <select id = "addFeatureType"></select></label>');
                        $('#addFeatureType').parent().after('<button id="newFeature">New Feature</button>');
                        $('#newFeatureDesc').parent().remove();
                        $('#newFeatureName').parent().remove();
                        $('#availfeature').remove();
                    }
                    $(this).dialog("close");
                },
            },
        });

    },

    editSeq: function() {
        $('#editSequenceOverlay').dialog({
            title: 'Edit Sequence',
            buttons: {
                "Confirm": function(){
                    $(this).dialog("close");
                },
                "Cancel": function() {
                    $("#edit_seq_text").empty();
                    $(this).dialog("close");
                },
            },

    });
    },

});
