/* Models for samples and experiments 
 *
 *
*/

window.Sample = Backbone.Model.extend({
    url: function () {
        return this.get('resource_uri') || this.colletion.url;
    },
    
    initialize: function () {
        _.bindAll(this);
    },

});


window.SampleCollection = Backbone.Collection.extend({
    model: Sample,
    url: '/weblab/api/v1/sample/',
    parse: function(data) {
        return data.objects;
    }
        

});
