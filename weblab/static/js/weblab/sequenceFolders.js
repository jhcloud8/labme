SeqFolder = Backbone.RelationalModel.extend({
    url: function() {
        return this.get('resource_uri') || this.collection.url;
    },

    relations: [
        {
            type: 'HasMany',
            key: 'sequences',
            relatedModel: 'seqRel',
            reverseRelation: {
                key: 'folderSeq'
            }
        }
    ],

    initialize: function() {
    }
});


seqRel = Backbone.RelationalModel.extend({
    defaults: {
    }
});


SeqFolderCollection = Backbone.Collection.extend({
    
    model: SeqFolder,
    
    url: '/weblab/api/v1/sequence_folder/',

    parse: function(data){
        return data.objects;
    }, 

        }
    
});
