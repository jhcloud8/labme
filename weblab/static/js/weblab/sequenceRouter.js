var SeqApp = Backbone.Router.extend({
    routes: {
        "" : "init"
    },

    initialize: function () {
        console.log('created')
    },
    
    init: function () {
        console.log('routed correctly');
        var sequences = new SeqCollection();
        sequences.fetch({
            success: function(data) {
                console.log('called')
                var seqview = new sequenceView({ el:$("#apps"), 
                                               collection: data});
            }
        })
    }
});
