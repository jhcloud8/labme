/* Javascript for managing biological samples
 *
 *
 */

(function($){

var SampleView = Backbone.View.extend({

    events: {
    },

    initialize: function () {
    },
    
    render: function () {
        $(this.el).html(ich.sample_template(this.model.toJSON()));
        return this;
    },
});

// Sample app
var SampleApp = Backbone.View.extend({
    el : $('#app'), 
    events : {
    },

    initialize: function () {
        _.bindAll(this, 'addOne', 'addAll', 'render');
        this.samples = new SampleCollection;
        this.samples.bind('add', this.addOne);
        this.samples.bind('refresh', this.addAll);
        this.samples.bind('all', this.render);
        this.samples.fetch();

    },

    addOne: function (sample) {
        console.log(sample)
        var view = new SampleView({model: sample});
        this.$('#samples').append(view.render().el);
    },

    addAll: function () {
        this.samples.each(this.addOne);
    },

    render: function () {
    },
});

})(jQuery);
