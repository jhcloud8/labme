function lrTraverse(node, nodeStop) {
    /*
     *
     */
    //Fix what if nodeStop is text?
    "use strict";
    var i, hit = 0, t_length = 0;
    for (i = 0; i < node.childNodes.length; i++) {
        //console.log(i);
        //console.log(node.childNodes[i]);
        if (node.childNodes[i] == nodeStop){
            //console.log('hit target');
            hit = 1;
            break
        } else {
            if (node.childNodes[i].nodeType == 3) {
                t_length = t_length + node.childNodes[i].length; 
            } else {
                //console.log("recursion");
                var result = lrTraverse(node.childNodes[i],nodeStop);
                t_length = t_length + result[0];
                if (result[1] == 1) {
                    break
                } else {
                }
            }
        }

    } 
    return [t_length, hit];
}


function makeSVG(tag, attrs){
    var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
        for (var k in attrs) {
            el.setAttribute(k, attrs[k])
        }
    return el
}
