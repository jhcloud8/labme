window.App = Backbone.View.extend({

    events: {
        'click .nseq': 'createSequence',
        'click .delete_sequence': 'deleteOne'
    },

    initialize: function () {
        _.bindAll(this, 'addOne', 'addAll', 'deleteOne', 'render');
        var self = this;
        this.sequences = new SeqCollection();
        this.sequences.bind('add', this.addOne);
        this.sequences.bind('refresh', function() {self.render();});
        this.sequences.bind('all', this.render);

    },

    addAll: function () {
        console.log(this.sequences);
        this.sequences.each(this.addOne);
    },

    addOne: function (sequence) {
        var view = new sequenceView({model:sequence});
        this.$('#sequences').append(view.render().el);
    },

    createSequence: function () {
        var seq_name = this.$('#seq_name').val();
        var seq_seq = this.$('#new_sequence').val();
        if (seq_name && seq_seq) {
            this.sequences.create({
                name: seq_name, 
                seq: seq_seq
            });
            this.$('#seq_name').val('');
            this.$('#new_sequence').val('');
        }
    }, 
    render: function () {

    },

    deleteOne: function () {
        //this.sequences.remove(currentEditing.model);
        console.log('clicked delete');
        $("#confirmOverlay").dialog({
            title: 'Confirm Delete',
            buttons: {
                "Yes": function() {
                    console.log('confirm clicked');
                    $(currentEditing.el).remove();
                    currentEditing.model.destroy();
                    $(this).dialog("close");
                },
                "No": function() {
                    $(this).dialog("close");
                },
            }

        });
        //Hmm the callback isn't being called for the removal of the UI element 

    },
});

