SeqFoldersView = Backbone.View.extend({
    events: {
        'click button.addSeq' : 'addSequence', 
        'click .addfold' : 'addFolder'
    },

    initialize: function () {
        _.bindAll(this, 'addOne', 'addAll', 'render', 'addSequence', 'addFolder');
        this.collection.bind('add', this.addOne);
        this.collection.bind('reset', this.addAll);
    },

    addOne: function(folder) {
        var view = new SeqFolderView({model:folder});
        this.$('#folders').append(view.render().el);
    }, 

    addAll: function() {
        this.collection.each(this.addOne);
    },

    addSequence: function () {
        var self = this;
        var options = $("#addSeqFoldField");
        var sfolders = new Array();
        options.empty();
        $.each(this.collection.models, function(index, folder) {
            options.append($("<option />").val(folder.cid).text(this.get('name')));
        });
        $("#addSequenceOverlay").dialog({
            width: 500,
            title: 'Add Sequence',
            buttons: {
                "Add": function() {
                    var sel_folder = $('#addSeqFoldField').val();
                    folder_conv = self.collection.getByCid(sel_folder);
                    sfolders[0] = folder_conv.get('resource_uri');
                    if (folder_conv.seq_coll_view != null) {
                    } else {
                        // Opens Folder if not open and renders it
                        // :Enhancement Open folder if not open, maybe make it a function since repeated
                        var seq_collections = new SeqCollection();
                        folder_conv.seq_coll_view = new SequencesView({ el: $(this.el), 
                                                                      collection: seq_collections});
                        seq_collections.fetch({data: {'folderSeq__name': folder_conv.get('name')}});
                    }
                    var temp = $(this);
                    folder_conv.seq_coll_view.collection.create({
                        name : $('#seq_name').val(),
                        alphabet : $('#seqType').val(),
                        seq : $('#new_sequence').val(),
                        folderSeq: sfolders,
                        keyword: $('#seq_key').val(),
                        description: $('#seq_desc').val(),
                        //features: '',
                        },
                        { 
                            success: function() {
                                $('#seq_name').val('');
                                $('#new_sequence').val('');
                                $('#seq_key').val('');
                                $('#seq_desc').val('');
                                $('#seq_errors').empty();
                                temp.dialog("close");
                            },
                            error: function(model, error, options){
                                var error = error.responseText;
                                $('#seq_errors').append('error');
                            },

                    });
                    // :TODO automatically set edit_seq to this new sequence
                },
                "Cancel": function () {
                    $(this).dialog("close");
                },
            },
        });
    },

    addFolder: function () {
        var self = this;
        $("#addFolderOverlay").dialog({
            title: 'Add Folder',
            buttons: {
                "Add": function() {
                    var foldname = $('#fold_name').val();
                    if (fold_name){
                        self.collection.create({
                            name : foldname,
                        });
                        $('#fold_name').val('');
                    }
                    $(this).dialog("close");
                },
                "Cancel": function () {
                    $(this).dialog("close");
                },
            },
        });

    },

});

SeqFolderView = Backbone.View.extend({
    tagName: 'li',
    className: 'seqFolder',

    events: {
        'click span.expand': 'expand'
    },

    initialize: function () {
        _.bindAll(this, 'render', 'expand');
        this.expanded = 0; 
        this.initialized = 0;
    },

    render: function() {
        $(this.el).html(ich.folderTemplate(this.model.toJSON()));
        return this
    },

    expand: function () {
        if (this.expanded == 0 && this.initialized == 0) { 
            var seq_collections = new SeqCollection();  
            this.model.seq_coll_view = new SequencesView({ el: $(this.el), collection: seq_collections});
            // :TODO default all folder
            seq_collections.fetch({data: {'folderSeq__id': this.model.get('id')}});
            this.expanded = 1;
            this.initialized = 1;
        } else if (this.expanded == 0) {
            this.model.seq_coll_view.addAll();
            this.expanded = 1;
        } else {
            this.model.seq_coll_view.unrender();
            this.expanded = 0
        }
    },

});
