window.Seq = Backbone.Model.extend({
    url: function () {
        return this.get('resource_uri') || this.collection.url;
    },
    initialize: function () {
        _.bindAll(this);
    }, 
    /*
    relations: [{
        type: Backbone.HasMany,
        key: 'featurelocation',
        relatedModel: 'FeatureLocation',
        collectionType: 'FeatureLocationCollection',
        reverseRelation: {
            key: 'sequence'
            },
    }]
    */

});

window.inFolder = Backbone.RelationalModel.extend({
    defaults: {
    }
});


window.SeqCollection = Backbone.Collection.extend({
    model: Seq,
    url: '/weblab/api/v1/sequence/',
    parse: function(data){
        return data.objects;
        }
});


window.SeqFolder = Backbone.RelationalModel.extend({
    url: function () {
        return this.get('resource_uri') || this.collection.url;
    },
    initialize: function () {
    },
    relations: [
        {
            type: 'HasMany',
            key: 'sequences',
            relatedModel: 'inFolder',
            reverseRelation: {
                key: 'folder'
            }
    }]
});


window.SeqFolderCollection = Backbone.Collection.extend({
    model: SeqFolder,
    url: '/weblab/api/v1/sequence_folder/',
    parse: function(data){
        return data.objects;
    }
});


window.SeqFeature = Backbone.Model.extend({
    url: function() {
        return this.get('resource_uri') || this.collection.url;
    }, 

    initialize: function () {
        _.bindAll(this);
    }, 
});


window.SeqFeatureCollection = Backbone.Collection.extend({
    model: SeqFeature, 
    url: '/weblab/api/v1/seq_feature/',
    parse: function(data){
        return data.objects;
    }

});

window.FeatureLocation = Backbone.Model.extend({
    url: function() {
        return this.get('resource_uri') || this.collection.url;
    },

    validate: function(attrs){
        if(attrs.end < attrs.start){
            return "end must come after start";
        }
    },

    initialize: function () {
        _.bindAll(this);
    },
    
});

window.FeatureLocationCollection = Backbone.Collection.extend({
    model: FeatureLocation,
    url: '/weblab/api/v1/featurelocation/',
    parse: function(data){
        return data.objects;
    }
});

