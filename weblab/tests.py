import warnings
import json

from django.test import TestCase
from django.utils import unittest
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.http import HttpRequest
from django.test.client import Client

from tastypie.authentication import BasicAuthentication
from tastypie.http import HttpUnauthorized

from weblab.models import Sequence, Sample, SequenceFolder
from weblab.api import SessionAuthentication


class AuthenticationTestCase(TestCase):
    """ Testing User login and registration against all the pages that require
    it.
    """

    def setUp(self):
        self.c = Client()
        self.demouser = User.objects.create_user('Demo', 'demo@test.com',
                                                 'pass')
    def testSequence(self):
        self.c.login(username="Demo", password='pass')
        response = self.c.post('/weblab/sequence/')
        self.assertEqual(response.status_code, 200)
        self.c.logout()
        response = self.c.post('/weblab/sequence/')
        self.assertEqual(response.status_code, 302)

    def testLogin(self):
        pass

    def testRegistration(self):
        pass


class SequenceTestCase(TestCase):
    """ Test server-side sequence checks
    """

    def setUp(self):
        self.demouser = User.objects.create_user('Demo', 'demo@test.com',
                                                 'pass')
        self.validDNA = Sequence.objects.create(name="validDNA",
                                              seq="AGCTAGACGATAGA",
                                                owner=self.demouser,
                                              alphabet=u'D')
'''
    def testSaveSequence(self):
        """ Custom save to remove spaces and checks validity
        """
        k = Sequence(name="New Sequence", seq = "AGT agt\ntga",
                     owner = self.demouser, alphabet=u'D')
        k.save()
        temp = Sequence.objects.filter(name__startswith='New Sequence')[0]
        self.assertEqual("AGTagttga", temp.seq)
'''


class SequenceApiTestsCase(TestCase):
    """ Test the API and make sure permissions and queries are set correctly
    """
    #fixtures = ['test.json']

    def setUp(self):
        self.c = Client()
        self.demouser = User.objects.create_user('Demo', 'demo@test.com',
                                                 'pass')
        self.privateuser = User.objects.create_user('Private', 'p@test.com',
                                                 'private')
        self.testFolder = SequenceFolder(owner=self.demouser,
                                         name="TestFolder")
        self.testFolder.save()
        self.privateFolder = SequenceFolder(owner=self.privateuser,
                                         name="PrivateFolder")
        self.privateFolder.save()
        """
        self.content_type_sequence = ContentType.objects.get(app_label='weblab',
                                                    model='Sequence')
        self.permission_seq =\
                Permission.objects.creat(codename = "weblab",
                                         model = 'Sequence',
                                         name = 'Can add seqs',
                                         content_type = \
                                         self.content_type_sequence)
        self.content_type_sF = ContentType.objects.get(app_label='weblab',
                                                       model='SequenceFolder')
        self.permission_sf =\
                Permission.objects.creat(codename = "weblab",
                                         model = 'SequenceFolder',
                                         name = 'Can add seq folders',
                                         content_type = \
                                         self.content_type_sF)
        """
        self.testSequence1 = Sequence()
        self.testSequence2 = Sequence()

    def test_is_authenticated(self):
        """ Make sure that user must be authenticated
        """
        auth = BasicAuthentication()
        request = HttpRequest()
        # No HTTP auth details should fail
        self.assertEqual(isinstance(auth.is_authenticated(request),
                                    HttpUnauthorized), True)

    def test_session_authentication(self):
        """ Test that authentication by session works correctly
        """
        from django.contrib.sessions.models import Session
        auth = SessionAuthentication()
        request = HttpRequest()
        self.c.login(username="Demo", password='pass')
        session = self.c.session
        session.save()
        request.COOKIES['sessionid'] = session.session_key
        self.assertEqual(isinstance(auth.is_authenticated(request),
                                    HttpUnauthorized), False)
        # :TODO need to test logout


    def test_owner_sequences(self):
        """ Ensures that only the sequences belonging the logged in user are
        returned.
        """
        pass

    def test_seq_folder_get(self):
        """ Testing sequence folder creation. Makes sure only the folders from
        the owner are received.
        """
        self.c.login(username="Demo", password='pass')
        response = self.c.get('/weblab/api/v1/sequence_folder/')
        seq_folder_test = json.loads(response.content)["objects"]
        self.assertEqual(len(seq_folder_test), 1)
        self.assertEqual(int(seq_folder_test[0]['id']),1)
        self.c.logout()
        self.c.login(username="Private", password='private')
        response = self.c.get('/weblab/api/v1/sequence_folder/')
        seq_folder_test = json.loads(response.content)["objects"]
        self.assertEqual(len(seq_folder_test), 1)
        self.assertEqual(int(seq_folder_test[0]['id']),2)
        session = self.c.session
        request = HttpRequest()
        request.COOKIES['sessionid'] = session.session_key
        print(self.demouser.has_perm('weblab.add_sequence'))
        print("%s is staff: %s" % (self.demouser ,self.demouser.is_staff))
        #self.demouser.permissions.add('weblab.add_sequencefolder')
        print(self.demouser.has_perm('weblab.add_sequencefolder'))

        del seq_folder_test[0]['id']
        print(seq_folder_test[0])
        print(json.dumps(seq_folder_test[0]))
        response = self.c.post('/weblab/api/v1/sequence_folder/',
                               data={"sequences": [], 
                                     "resource_uri":\
                                     "/weblab/api/v1/sequence_folder/2/", 
                                     "name": "PrivateFolder"},
                               headers={'content-type': 'application/json'})
        print(response)
        print(response.status_code)

