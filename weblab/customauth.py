from tastypie.authentication import Authentication
from tastypie.authorization import Authorization
from weblab.models import Sample

class SampleAuthentication(Authentication):
    def is_authenticated(self, request, **kwargs):
        pass 

    def get_identifier(self, request):
        return request.user.username

class SampleAuthorization(Authorization):

    def apply_limits(self, request, object_list):
        if request and hasattr(request, 'user'):
            pass
        return object_list.none()
