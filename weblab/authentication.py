from tastypie.authentication import BasicAuthentication

class SessionAuthentication(BasicAuthentication):
    def __init__(self, *args, **kwargs):
        super(SessionAuthentication, self).__init__(*args, **kwargs)

    def is_authenticated(self, request, **kwargs):
        from django.contrib.sessions.models import Session
        if 'sessionid' in request.COOKIES:
            s = Session.objects.get(pk=request.COOKIES['sessionid'])
            if '__auth_user_id' in s.get_decoded():
                u = User.objects.get(id=s.get_decoded()['_auth_user_id'])
                request.user = u
                return True
        return super(SessionAuthentication, self).is_authenticated(request,
                                                                   **kwargs)
