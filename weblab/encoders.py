import json

def folder_to_dict(sequenceFolder):
    """ Depends on the folder class
    """
    return {
        'name': sequenceFolder.name,
        'owner': sequenceFolder.owner,
        'sequences': sequenceFolder.sequences,
    }

def folders_to_json(sequenceFolders):
    response = {
        'success': True,
        'results': len(sequenceFolders),
        'objects': [folder_to_dict(folder) for folder in\
                            sequenceFolders]
    }

    return json.dumps(response)
