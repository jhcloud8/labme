from django.contrib.auth.models import User

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie import fields
from tastypie.validation import CleanedDataFormValidation

from weblab.models import (Sample, Reagent, Lab, Sequence, SequenceFolder,
                           Storage, SeqFeature, FeatureLocation)
from weblab.forms import SequenceForm


class SessionAuthentication(BasicAuthentication):
    def __init__(self, *args, **kwargs):
        super(SessionAuthentication, self).__init__(*args, **kwargs)

    def is_authenticated(self, request, **kwargs):
        from django.contrib.sessions.models import Session
        if 'sessionid' in request.COOKIES:
            s = Session.objects.get(pk=request.COOKIES['sessionid'])
            if '_auth_user_id' in s.get_decoded():
                u = User.objects.get(id=s.get_decoded()['_auth_user_id'])
                request.user = u
                #print('_auth_user_id found: %s' % u)
                return True
        return super(SessionAuthentication, self).is_authenticated(request,
                                                                   **kwargs)

class customMany(fields.ToManyField):
    def dehydrate(self, bundle):
        if not bundle.obj or not bundle.obj.pk:
            if not self.null:
                raise ApiFieldError("This model '%r' does not have a primary\
                                    key and can not be used in a ToMany\
                                    context." % bundle.obj)
                return []

        the_m2ms = None
        if isinstance(self.attribute, basestring):
            the_m2ms = getattr(bundle.obj, self.attribute)
        elif callable(self.attribute):
            the_m2ms = self.attribute(bundle)
        if not the_m2ms:
            if not self.null:
                print("here")
                raise ApiFieldError("Welp")
            return []

        self.m2m_resources = []
        m2m_dehydrated = []

        for m2m in the_m2ms.all():
            m2m_resource = self.get_related_resource(m2m)
            print(m2m_resource)

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_active', 'is_staff',
        'is_superuser']
        #User Identification
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()


class SampleResource(ModelResource):
    class Meta:
        queryset = Sample.objects.all()
        resource_name = 'sample'
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(owner=request.user)
    
    def obj_create(self, bundle, request=None, **kwargs):
        return super(SequenceResource, self).obj_create(bundle, request,
                                                        owner=request.user)





class LabResource(ModelResource):
    class Meta:
        queryset = Lab.objects.all()
        resource_name = 'lab'
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()



class SequenceResource(ModelResource):
    folderSeq = fields.ToManyField('weblab.api.SequenceFolderResource',
                             'folder_seq')
    featurelocations = fields.ToManyField('weblab.api.FeatureLocationResource',
    #featurelocations = customMany('weblab.api.FeatureLocationResource',
                                     'featurelocations',null=True, full=True)
    #owner = fields.ToOneField(UserResource, 'owner')
    class Meta:
        queryset = Sequence.objects.all()
        resource_name = 'sequence'
        list_allowed_methods = ['get', 'post']
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()
        filtering = {
            "name": ('exact', 'startswith'),
            "folderSeq": ALL_WITH_RELATIONS,
        }
        validation = CleanedDataFormValidation(form_class=SequenceForm)

    def obj_create(self, bundle, request=None, **kwargs):
        return super(SequenceResource, self).obj_create(bundle, request,
                                                        owner=request.user)

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(owner=request.user)


    def dehydrate_featurelocations(self, bundle):
        return bundle.data['featurelocations']


    def save_m2m(self, bundle):
        """ Handles the through case with feature locations
        """
        for field_name, field_object in self.fields.items():
            if not getattr(field_object, 'is_m2m', False):
                continue
            if not field_object.attribute:
                continue
            if field_object.readonly:
                continue
            if field_name == 'featurelocations': # Override saving through model
                continue

            related_mngr = getattr(bundle.obj, field_object.attribute)

            if hasattr(related_mngr, 'clear'):
                related_mngr.clear()

            related_objs = []

            for related_bundle in bundle.data[field_name]:
                related_bundle.obj.save()
                related_objs.append(related_bundle.obj)

            related_mngr.add(*related_objs)



class SequenceFolderResource(ModelResource):
    sequences = fields.ToManyField(SequenceResource, 'sequences', null=True)
    class Meta:
        queryset = SequenceFolder.objects.all()
        resource_name = 'sequence_folder'
        list_allowed_methods = ['get', 'post']
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()
        filtering = {
            "name": ('exact', 'startswith'),
            "id": ('exact'),
        }

    def obj_create(self, bundle, request=None, **kwargs):
        return super(SequenceFolderResource, self).obj_create(bundle, request,
                                                           owner=request.user)
    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(owner=request.user)


class SeqFeatureResource(ModelResource):
    '''
    feature_location =\
            fields.ToManyField('weblab.api.FeatureLocationResource',
                               'featurelocation_set')
    '''
    class Meta:
        queryset = SeqFeature.objects.all()
        resource_name = 'seq_feature'
        list_allowed_methods = ['get', 'post']
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()
        filtering = {
            "name": ('exact', 'startswith'),
            "id": ('exact'),
        }

    def obj_create(self, bundle, request=None, **kwargs):
        return super(SeqFeatureResource, self).obj_create(bundle, request,
                                                        owner=request.user)

    def obj_update(self, bundle, request=None, **kwargs):
        print(bundle)
        return super(SeqFeatureResource,self).obj_create(bundle, request,
                                                         owner=bundle.data['owner'])

    """
    def apply_authorization_limits(self, request, object_list):
        # Maybe no authorization and have it always available
        return object_list.filter(owner=request.user)
    """



class FeatureLocationResource(ModelResource):
    sequence = fields.ForeignKey('weblab.api.SequenceResource',
                             'seq')
    feature = fields.ForeignKey('weblab.api.SeqFeatureResource',
                                     'feature', full=True)
    class Meta:
        queryset = FeatureLocation.objects.all()
        resource_name = 'featurelocation'
        list_allowed_methods = ['get', 'post', 'update']
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()

    def dehydrate(self, bundle):
        return(bundle)

    def hydrate(self,bundle):
        if bundle.data['feature']:
            print(bundle.request.user)
            print(type(bundle.data['feature']))
            # :BUG bundle.data['feature'] is not a dictionary
            #bundle.data['feature']['owner'] = bundle.request.user
        return(bundle)



