import datetime
from haystack import indexes
from weblab.models import Sample

class SampleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    owner = indexes.CharField(model_attr='owner')
    created = indexes.DateTimeField(model_attr='created')

    def get_model(self):
        return Sample

    def index_queryset(self):
        """ Used when the entire index for the model is updated."""
        return Sample.objects.filter(created__lte=datetime.datetime.now())

