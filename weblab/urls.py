from django.conf.urls.defaults import *
from django.contrib import admin
from tastypie.api import Api
from weblab.api import (SampleResource, UserResource, LabResource,
                        SequenceResource, SequenceFolderResource,
                        FeatureLocationResource, SeqFeatureResource)


v1_api = Api(api_name = 'v1')
v1_api.register(SampleResource())
v1_api.register(UserResource())
v1_api.register(LabResource())
v1_api.register(SequenceResource())
v1_api.register(SequenceFolderResource())
v1_api.register(FeatureLocationResource())
v1_api.register(SeqFeatureResource())


urlpatterns = patterns(
        '',
        (r'^locations', "weblab.views.locations"),
        (r'^reagents', "weblab.views.reagents"),
        (r'^samples', "weblab.views.samples"),
        (r'^animals', "weblab.views.animals"),
        (r'^signout/', "weblab.views.signout"), 
        (r'^developers/', 'django.views.generic.simple.direct_to_template',
         {'template':'developers.html'}),
        (r'^about', "weblab.views.about"),
        (r'^api/', include(v1_api.urls)),
        (r'^sample_request', "weblab.views.sample_request"),
        (r'^search/', include('haystack.urls')),
        (r'^accounts/', include('registration.backends.default.urls')),
        (r'^experiment/', "weblab.views.experiment"),
        (r'^sequence/', "weblab.views.sequences"),
        (r'^$', "weblab.views.index"),
        )
