import json

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout

from weblab.models import Sample, Location, Storage, Experiment
from weblab.models import SequenceFolder
from weblab.forms import ExperimentSubmissionForm
from weblab.encoders import folders_to_json

def index(request):
    return render_to_response('weblab.html',
            context_instance=RequestContext(request))

@login_required(login_url='/weblab/login/')
def locations(request):
    return render_to_response('locations.html',
            context_instance=RequestContext(request))

@login_required(login_url='/weblab/accounts/login/')
def reagents(request):
    return render_to_response('reagents.html',
                              context_instance=RequestContext(request))

def about(request):
    return render_to_response('about.html', 
                              context_instance=RequestContext(request))

def developers(request):
    return render_to_response('developers.html',
                              context_instance=RequestContext(request))

def clients(request):
    return render_to_response('clients.html',
                              context_instance=RequestContext(request))

def animals(request):
    pass

@login_required(login_url='/weblab/accounts/login/')
def samples(request):
    """ Contains a view for both animal and other samples
    :TODO have function to return next set of samples
    """
    if request.user.is_authenticated():
        samples = Sample.objects.filter(viewable=request.user) 
    else:
        # Should I display public samples
        samples = Sample.objects.order_by("-created")[0:10]
    return render_to_response('samples.html', {'samples': samples,},
            context_instance=RequestContext(request))

def sample_detail(request, sample_id):
    sample = get_object_or_404(Sample, pk=sample_id)
    return render_to_response('sample_detail.html', {'sample': sample},
            context_instance=RequestContext(request))


def signout(request):
    logout(request)
    return HttpResponseRedirect('/weblab/')


def experiment(request):
    """ Contains view to add samples to experiments
    """
    if request.POST:
        print(request.POST)
        form = ExperimentSubmissionForm(request.POST)
        print(form.errors)
        if form.is_valid():
            data = form.cleaned_data
            exp = Experiment(name = data.get("name"))
            print(data)
            #exp.save()
            #exp.samples.add(sample)
            return HttpResponseRedirect('thanks.html')
        else: print("what")
    else:
        form = ExperimentSubmissionForm()
        return render_to_response('experiment.html',{'form': form},
                context_instance=RequestContext(request))


def sample_request(request):
    results = []
    if request.method == 'GET':
        if request.GET.has_key(u'term'):
            value = request.GET[u'term']
            # Makes user has inputted at least 3 letters
            if len(value) > 2:
                sample_results = Sample.objects.filter(name__icontains=value)
                results = [ (x.__unicode__(), x.id) for x in sample_results ]
    response = json.dumps(results)
    return(HttpResponse(response, mimetype="application/json"))


@login_required(login_url='/weblab/accounts/login/')
def sequences(request):
    """ Initial pageload includes all data, no need to make an additional ajax
    calls.
    """
    return render_to_response('sequences.html',
                              context_instance=RequestContext(request))
