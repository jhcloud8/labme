from django import forms
from haystack.forms import SearchForm
from weblab.models import Sample, Sequence, SequenceFolder


class SequenceField(forms.Field):
    def to_python(self, value):
        # Do this here or elsewhere?
        value = str(value)
        value = value.replace(" ", "").replace("\n", "")
        return(value)

    def validate(self, value):
        """Check if value consists of only valid sequences"""
        super(SequenceField, self).validate(value)


class TastypieM2MField(forms.Field):
    """ Custome validation for tastypie many to many uri relationships
    """
    def to_python(self, value):
        if value:
            value = list(value)
            return(value)
        else:
            return(u'')


class ExperimentSubmissionForm(forms.Form):
    name = forms.CharField(max_length = 255, initial="" )
    samples = forms.CharField(label='samples',
            widget=forms.TextInput(attrs={'placeholder':'Sample Name'}))
    sample_pk = forms.IntegerField(widget=forms.MultipleHiddenInput())


class BasicSearchBar(SearchForm):
    def search(self):
        sqs = super(BasicSearchBar, self).search()
        return sqs

class SampleForm(forms.Form):
    name = forms.CharField(max_length = 255, initial="")
    folder = forms.CharField(max_length = 255, initial="")


class SequenceForm(forms.Form):
    name = forms.CharField(max_length=250)
    seq = SequenceField()
    alphabet = forms.CharField(max_length=1)
    keyword = forms.CharField(max_length=400, required=False)
    description = forms.CharField(max_length=20000, required=False)
    folderSeq = TastypieM2MField()
    featurelocations = TastypieM2MField(required=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        print(cleaned_data)
        seq = cleaned_data.get("seq")
        alphabet = cleaned_data.get("alphabet")

        if alphabet == u'P':
            pass
        elif alphabet == u'D':
            d = ('A','G','C','T','N')
            if seq:
                for letter in seq:
                    if letter.upper() not in d:
                        msg = u"Invalid letter in DNA sequnce"
                        self._errors["seq"] = self.error_class([msg])
                        self._errors["alphabet"] = self.error_class([msg])
                        del cleaned_data["seq"]
                        del cleaned_data["alphabet"]
                        break
                    else: pass
            else:
                msg = u"Must have sequence"
                self._errors["seq"] = self.error_class([msg])

        return cleaned_data

class FeatureLocationForm(forms.Form):
    start = forms.IntegerField()
    end = forms.IntegerField()

    def clean(self):
        # Checks to make sure start and end lie within the sequence
        pass
