from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator

# Accelrating Science, Medicine and Industrial Chemical Managment 

# :TODO add default none storage


class Location(models.Model):
    """
    Location is room/building/lab/university etc.
    In addition contains a JSON field that has the layout of the location,
    including objects such as storage and machines.  
    """
    name = models.CharField(max_length=250)
    description = models.TextField('additional details')

    def __unicode__(self):
        return(self.name)


class Storage(models.Model):
    """
    Storage is an identifier for anything that contain multiple samples or
    reagents. 
    """
    name = models.CharField(max_length=250)
    location = models.ForeignKey(Location, default=None, null=True, blank=True,
            on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)
    parent_storage = models.ForeignKey('self', default=None, null=True,
                                       blank=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return(self.name[:30])

class Sample(models.Model):
    """ Samples are unique
    Should sample names be unique?
    """
    sub_date = models.DateTimeField('submission date')
    storage = models.ForeignKey(Storage,default=None, null=True, blank=True,
            on_delete=models.SET_NULL)
    name = models.CharField(max_length=250)
    owner = models.ForeignKey(User, related_name='sample_owner', default=None,
            null=True)
    viewable = models.ManyToManyField(User, default=None,blank=True, 
            related_name='sample_viewable', db_column='viewable_id')
    description = models.TextField('additional details')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return(self.name)


class Animal(models.Model):
    """ Subclass of samples
    """
    name = models.CharField(max_length=250)
    date_of_birth = models.DateTimeField('date of animal')
    strain = models.CharField(max_length=250)
    sac_date = models.DateTimeField('Sacrifice date of animal')

    def __unicode__(self):
        return(self.name)


class Strain(models.Model):
    """ Strain of a biological sample
    """
    pass

class Vendor(models.Model):
    """ Vendor API
    """
    vendor_name = models.CharField(max_length = 250)

    def __unicode__(self):
        return(self.name)

class Reagent(models.Model):
    """Chemicals or materials used for analysis.
    """
    name = models.CharField(max_length = 250)
    # :TODO have a pre-generate list ov vendors
    vendor = models.ForeignKey(Vendor)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

class Assay(models.Model):
    """A biological assay
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Experiment(models.Model):
    """A class defining an experiment and associated samples
    """
    name = models.CharField(max_length = 250)
    description = models.TextField('additional details') 
    samples = models.ManyToManyField(Sample)
    created = models.DateTimeField(auto_now_add=True)


    def __unicode__(self):
        return(self.name)


class Lab(models.Model):
    """
    Model for individual laboratories, helps for setting permissions.
    """
    labname = models.CharField(max_length=200)
    members = models.ManyToManyField(User, related_name='lab_member')
    managers = models.ManyToManyField(User, related_name='lab_manager')

    def __unicode__(self):
        return(self.labname)

class Protocol(models.Model):
    """ Established protocols
    """
    pass


class SeqFeature(models.Model):
    """ A sequence feature such as a restriction site, gene, promoter,
    enhancer etc.
    """
    name = models.CharField(max_length=250
)
    seq = models.CharField(max_length=10000, null=True, blank=True)
    description = models.TextField()
    owner = models.ForeignKey(User, related_name='feat_owner',
                              default=None, blank=True, null=True)

    def __unicode__(self):
        return(self.name)


class Sequence(models.Model):
    """
    Generic sequences
    :TODO need to set a max sequence
    """
    KNOWN_ALPHABET = ((u'D', 'DNA'), (u'P','Protein'), (u'R', 'RNA'))
    seq = models.TextField('GenericSequence',
                           validators=[MaxLengthValidator(40000)])
    name = models.CharField(max_length=250)
    keyword = models.CharField(max_length=400, blank=True)
    description = models.TextField(max_length=20000, blank=True)
    owner = models.ForeignKey(User, related_name='seq_owner', default=None)
    alphabet = models.CharField(max_length=1, choices=KNOWN_ALPHABET)
    features = models.ManyToManyField(SeqFeature, through="FeatureLocation",
                                     null = True, blank = True)
    # Need to validate that feature_s and feature_e are the same length


    def __unicode__(self):
        return(self.name)

    def isValid(self):
        """ Server-side check on sequence similarity
        """
        self.seq = self.seq.strip()
        if self.alphabet == u'P':
            pass
        elif self.alphabet == u'D':
            d = ('A','G','C','T','a','g','c','t', 'N', 'n')
            for x in self.seq:
                if x not in d:
                   return(False)
            return(True)



class FeatureLocation(models.Model):
    """ Through table for feature sequence location. This should still have a
    name.
    """
    name = models.CharField(max_length=250)
    seq = models.ForeignKey(Sequence, related_name='featurelocations')
    feature = models.ForeignKey(SeqFeature, blank=True, null=True)
    # 0 for + strand 1 for - strand
    orientation = models.IntegerField(max_length = 1, blank=True, null=True)
    start = models.IntegerField()
    end = models.IntegerField()

    def __unicode__(self):
        #return('what')
        return(self.seq.name + "-" + self.feature.name)


class SequenceFolder(models.Model):
    """ User-organized sequences and samples
    """
    name = models.CharField(max_length=250)
    owner = models.ForeignKey(User, related_name='sequence_owner', default=None)
    # :FIX Change this to One to One?
    sequences = models.ManyToManyField(Sequence, related_name='folder_seq',
                                       default=None, blank=True)

    def __unicode__(self):
        return(self.name)

    def save(self, *args, **kwargs): 
        super(SequenceFolder, self).save(*args, **kwargs) 

