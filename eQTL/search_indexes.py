import datetime
from haystack import indexes
from eQTL.models import QTL

class QTLIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return QTL

    def index_queryset(self):
        """ Used when the entire index for the model is updated."""
        return QTL.objects.filter()

