from eQTL.models import QTL, Assay_ID
from django.contrib import admin

class QTLAdmin(admin.ModelAdmin):
    exclude = ('kbin',)

class Assay_IDAdmin(admin.ModelAdmin):
    exclude = ()

admin.site.register(QTL, QTLAdmin)
admin.site.register(Assay_ID, Assay_IDAdmin)

