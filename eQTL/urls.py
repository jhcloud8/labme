from django.conf.urls import *
from django.contrib import admin
from tastypie.api import Api
from haystack.forms import ModelSearchForm
from haystack.query import SearchQuerySet
from haystack.views import SearchView, search_view_factory
from eQTL.api import eQTLResource


v1_api = Api(api_name = 'das')
v1_api.register(eQTLResource())
sqs=SearchQuerySet()

urlpatterns = patterns('',
        (r'^api/', include(v1_api.urls)),
        (r'^search/', search_view_factory(
            view_class=SearchView,
            template="search/eQTLsearch.html",
            searchqueryset=sqs,
            form_class=ModelSearchForm)),
        (r'^test/', "eQTL.views.test_view"),
        (r'^test2/', "eQTL.data_views.test_view"),
        (r'^assay_id/ILMN_(?P<assay_id>\d+)/$', 'eQTL.views.assay_id'),
        (r'^das/eQTL/features$','eQTL.views.das'),
        (r'^das/sources$', 'eQTL.views.dassources'),
        #(r'^das/eQTL/', 'eQTL.views.empty'),
        (r'^das/types$', 'eQTL.views.types'),
        (r'^das/eQTL/stylesheet', 'eQTL.views.stylesheet'),
        (r'^das/eQTL/features/stylesheet', 'eQTL.views.stylesheet'),
        (r'^$', "eQTL.views.index"),
        )

