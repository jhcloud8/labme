from django.db import models

binOffsets = [512+64+8+1, 64+8+1, 8+1, 1, 0]
_BINFIRSTSHIFT = 17
_BINNEXTSHIFT = 3

class Experiment(models.Model):
    tissue = models.CharField(max_length = 20)


class QTL(models.Model):
    """
    """
    EQTL_TYPE = (('NA', 'unknown'),
                 ('cis', 'cis-eQTL'),
                 ('trans', 'trans-eQTL'))
    GENDER = ((u'M', 'Male'), (u'F', 'Female'), (u'B', 'Both'), (u'N', 'NA'))
    kbin = models.IntegerField(null=True, blank=True)
    chrom = models.IntegerField()
    start = models.IntegerField()
    end = models.IntegerField()
    gene = models.CharField(max_length=300)
    lodscore = models.FloatField()
    assay_type = models.CharField(max_length=40)
    gender = models.CharField(max_length=1)
    eqtl_type = models.CharField(max_length=4)
    marker = models.CharField(max_length = 30)
    tissue = models.CharField(max_length = 40)

    def __unicode__(self):
        return(self.gene)

    def save(self):
        """ Calculates the Kent bin if there isn't one already
        """
        # :TODO need to check if start or end are greater than 512M
        if self.kbin == None:
            global _BINFIRSTSHIFT, _BINNEXTSHIFT, binOffsets
            startBin = self.start
            endBin = self.end - 1
            startBin >>= _BINFIRSTSHIFT
            endBin >>= _BINFIRSTSHIFT
            for i in binOffsets:
                if startBin == endBin:
                    kbin = i + startBin
                    break
                else: pass
                startBin >>= _BINNEXTSHIFT
                endBin >>= _BINNEXTSHIFT
            self.kbin = kbin
            super(QTL, self).save()
        else:
            super(QTL, self).save()

class Assay_ID(models.Model):
    """ Table for Illumina 
    """
    probe_id = models.IntegerField(primary_key = True)
    seq = models.CharField(max_length = 51)
    chrom = models.IntegerField(max_length=3)
    start = models.IntegerField(max_length=9, blank=True)
    symbol = models.CharField(max_length=300)
    snp = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return("ILMN_" + str(self.probe_id))

