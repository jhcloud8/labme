from eQTL.models import QTL, Assay_ID

def main():
    import optparse
    p = optparse.OptionParser(__doc__)
    p.add_option("-f", "--file", dest="file", help="")
    p.add_option("-s", "--skip", dest="skip", help="")

    options, args = p.parse_args()
    fh = open(args[0], 'rU')
    fh.next()
    for line in fh:
        line = line.rstrip("\n").split("\t")
        try:
            chromosome = int(line[6])
            q = QTL(chrom = chromosome, start = int(line[7]),
                    end = int(line[7]), gene = line[8],
                    lodscore = float(line[5]),
                    assay_type = line[1],
                   gender = u'M', eqtl_type= line[13],
                   marker = line[4])
            q.save()
        except ValueError:
            print(" File is malformed ")

def create_header_dict(header_line):
    """ Create a header dictionary.  

    Header must contain the following items:
        'Probe_ID'
        'qtl_names'
        'qtl_marker_pos'
        'lodscore'
        'qtl_chromosomes'
    """
    required = ['Probe_ID', 'qtl_names', 'qtl_chromosomes', 'qtl_marker_pos',
                'gene_symbol', 'cisstatus', 'lodscore']
    header_line = header_line.lstrip("#").rstrip("\n")
    header_line = header_line.split("\t")
    headerd = {}
    print(header_line)
    for i in required:
        print(i)
        headerd[i]= header_line.index(i)+1
    return(headerd)


def from_shell(filepath, tissue="BMM"):
    fh = open(filepath, 'rU')
    # All eQTL files have a header starting with a # line
    header = create_header_dict(fh.next())
    for line in fh:
        line = line.rstrip("\n").split("\t")
        try:
            chromosome = int(line[header['qtl_chromosomes']])
        except ValueError:
            chromosome = line[header['qtl_chromosomes']]
            if chromosome == 'X':
                chromosome = 20
            elif chromosome == 'Y':
                chromosome = 21
        q = QTL(chrom = chromosome,
                start = int(line[header['qtl_marker_pos']]),
                end = int(line[header['qtl_marker_pos']]),
                gene = line[header['gene_symbol']],
                lodscore = float(line[header['lodscore']]),
                assay_type = line[header['Probe_ID']],
                gender = u'M',
                eqtl_type = line[header['cisstatus']],
                marker = line[header['qtl_names']],
                tissue = tissue
                )
        q.save()

def probe_annot(filepath):
    with open(filepath, 'rU') as fh:
        fh.next()
        for line in fh:
            try:
                line = line.strip('\n').split("\t")
                symbol = line[11]
                seq = line[17]
                chrom = line[18]
                try:
                    pid = int(line[13].lstrip("ILMN_"))
                    start = int(line[20].split("-")[0])
                    print(symbol, pid, seq, chrom, start)
                    annot = Assay_ID(symbol = line[10],
                                     seq = seq, chrom = chrom,
                                     probe_id = pid, start = start)
                    annot.save()
                except ValueError:
                    pass

            except IndexError:
                pass


if __name__ == '__main__':
    main()
