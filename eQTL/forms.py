from django import forms
from haystack.forms import SearchForm
from eQTL.models import QTL

class QTLForm(forms.Form):
    gene = forms.CharField(max_length = 300, initial="")

class eQTLSearchForm(SearchForm):
    def search(self):
        sqs = super(eQTLSearchForm, self).search()
        return sqs

class DASSourcesForm(forms.Form):
    capability = forms.CharField(max_length =100, initial="", required=False)
    type = forms.CharField(max_length = 100, initial="", required=False)
    authority = forms.CharField(max_length = 100, required=False)
    version = forms.CharField(max_length = 100, required=False)
    organism = forms.CharField(max_length = 100, required=False) 
    label = forms.CharField(max_length = 100, required=False)

class TypeForm(forms.Form):
    segment = forms.CharField(max_length = 100)
    type = forms.CharField(max_length = 100)

