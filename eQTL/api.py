from django.conf.urls import url

from tastypie.resources import ModelResource
from tastypie import fields

from eQTL.models import QTL

class eQTLResource(ModelResource):
    class Meta:
        queryset = QTL.objects.all()
        excludes = ['lodscore', 'kbin', 'assay_type', 'id', 'gender',
                    'marker', 'tissue']
