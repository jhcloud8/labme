# Create your views here.

from django.shortcuts import (render_to_response, get_object_or_404,
                              render)
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse

from eQTL.models import QTL, Assay_ID
from eQTL.forms import QTLForm, DASSourcesForm, TypeForm

def index(request):
    return render_to_response('eQTL.html',
                             context_instance=RequestContext(request))

def search_eQTL(request):
    if request.POST:
        form = QTLForm(requst.POST)
        if form.is_valid():
            data = form.cleaned_data
            QTL = QTL.objects.filter(gene = data.get("gene"))
        return render_to_response('eTLfound.hml')


def test_view(request):
    response = HttpResponse()
    response['Content-Type'] = 'application/x-das-features+xml; version=300'
    return response


def assay_id(request, assay_id):
    assay = Assay_ID.objects.get(probe_id=int(assay_id))
    return render_to_response('assay.html', {'assay':assay})


def add_das_headers(response):
    response['Content-Type'] = 'text/xml'
    response['X-DAS-Version'] = 'DAS/1.6'
    response['X-DAS-Status'] = 400 
    response['X-DAS-Capabilities'] = 'features/1.0;'
    response['X-DAS-Server'] = 'DjangoDas'
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Expose-Headers'] = 'X-DAS-Version,X-DAS-Status,X-DAS-Capabilities, X-DAS-Server'
    return response


def das(request):
    if request.POST:
        seg = request.POST.get('segment')
        try:
            chrom = int(seg.split(":")[0])
            start = int(seg.split(":")[1].split(",")[0])
            end = int(seg.split(":")[1].split(",")[1])
            print(chrom, start, end)
            # Generalize this section
            qtls =\
                    QTL.objects.filter(chrom=chrom).exclude(
                        start__gt=end).exclude(
                            end__lt=start)
            status_code = 200
            response = render_to_response('das.html', {'qtls':qtls,
                                                       'seg':chrom})
        except:
            status_code = 500
            response = render_to_response('das.html')
    else:
        seg = request.GET.get('segment')
        try:
            chrom = int(seg.split(":")[0])
            start = int(seg.split(":")[1].split(",")[0])
            end = int(seg.split(":")[1].split(",")[1])
            print(chrom, start, end)
            qtls =\
                    QTL.objects.filter(chrom=chrom).exclude(
                        start__gt=end).exclude(
                            end__lt=start)
            status_code = 200
            response = render_to_response('das.html', {'qtls':qtls,
                                                       'seg':chrom,
                                                        'url':'what'})
        except:
            status_code = 500
            response = render_to_response('das.html')
    response = add_das_headers(response)
    return response

def dassources(request):
    if request.POST:
        form = DASSourcesForm(request.POST)
    elif request.GET:
        form = DASSourcesForm(request.GET)
    else:
        form = DASSourcesForm(request.GET)
    print(form.errors)
    if form.is_valid():
        data = form.cleaned_data
    response = render_to_response('das_sources.html')
    response = add_das_headers(response)
    return response

def types(request):
    if request.POST:
        form = TypeForm(request.POST)
    elif request.GET:
        form = TypeForm(request.GET)
    if form.is_valid():
        pass
    response = render_to_response('das_sources.html')
    response = add_das_headers(response)
    return response

def stylesheet(request):
    response = render_to_response('stylesheet.xml')
    response = add_das_headers(response)
    return response
