.. weblab documentation master file, created by
   sphinx-quickstart on Sun Jan 15 11:24:19 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to weblab's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2
   intro
   sequences api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

