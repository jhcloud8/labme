# Create your views here.

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse


def genome(request):
    return render_to_response('genome.html', 
            context_instance=RequestContext(request))


def mouse_genome(request):
    return render_to_response('mousegenome.html', 
            context_instance=RequestContext(request))
