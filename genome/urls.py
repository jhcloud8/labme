from django.conf.urls.defaults import patterns, include, url
from genome import views

urlpatterns = patterns('genome.views',
        (r'^mm9', 'mouse_genome'),
        (r'^$', 'genome'),
        )
