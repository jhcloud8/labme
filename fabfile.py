from fabric.api import *

env.hosts = ['jhcloud@jhsu.org']
def pushpull():
    """Push local repo and pull from remote
    """
    local('git push jhcloud master')
    local('scp settings.py jhsu:production/labme/')
    run('cd /home/jhcloud/production/labme; git pull')

def staticfiles():
    """ Deal with static files
    """

    local('python manage.py collectstatic')
    local('rsync -rzptL -e "ssh -i /home/jeff/.ssh/jeff-desktop-rysnc-key"\
          ../static/ jhcloud@173.255.238.176:/home/jhcloud/static/')

def startserver():
    run('cd /home/jhcloud/production/labme;\
        python2 manage.py runfcgi method=threaded host=127.0.0.1 port=8802')

def buildindexes():
    """ Builds the search indexes for django-tastypie
    """
    run(' cd /home/jhcloud/production/labme;\
        python2 manage.py rebuild_index --noinput')

